# OAuth Sample Code

This repository contains simple programs demonstrating how to make use of OAuth-based user impersonation in Atlassian Connect.
It is a companion to [this documentation](https://developer.atlassian.com/static/connect/docs/latest/concepts/OAuth2-JWT-Bearer-Token-Authentication.html).

## Pre-requisites

You will need an add-on with the `ACT_AS_USER` scope, and access to that add-on's installation data (so you can retrieve
the secret and `oauthClientId` to provide to the sample program).

## Contents

### `java`

A standalone java program demonstrating how to obtain and use a token from the authorization service. To install and run (requires JDK 8):

```
cd java
javac OAuthTokenGetter.java
java OAuthTokenGetter [arguments]
```

See the comments in `OAuthTokenGetter.java` or run the class without arguments for more information.

### `javascript`

A javascript program also demonstrating how to obtain and use a token. It requires node and npm. To install and run (requires node >= v0.12.2:

```
cd javascript
npm install
node . [arguments]
```

See the comments at the top of `index.js` or run the program without arguments for more information.
